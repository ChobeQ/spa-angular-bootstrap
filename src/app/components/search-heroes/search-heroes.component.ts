import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-search-heroes',
  templateUrl: './search-heroes.component.html',
  styleUrls: ['./search-heroes.component.css']
})
export class SearchHeroesComponent implements OnInit {

  heroes:any[] = [];
  termino:string;

  constructor( private _heroesServices:HeroesService,
               private activatedRoute:ActivatedRoute,
               private router:Router
               ) { }
  
  ngOnInit() {
    this.activatedRoute.params.subscribe( params =>{
      this.termino = params['termino'];
      this.heroes = this._heroesServices.buscarHeroes( params['termino'] );
    });
  }

  verHeroe(idx:number){
    this.router.navigate(['/heroe',idx]);
  }

}
